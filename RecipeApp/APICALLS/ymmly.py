#https://rapidapi.com/category/Food
#https://rapidapi.com/apidojo/api/yummly2/
import requests
import json

def ymmly_list():
    url = "https://yummly2.p.rapidapi.com/feeds/list"
    querystring = {"limit":"1","start":"0"}
    headers = {
        "X-RapidAPI-Host": "yummly2.p.rapidapi.com",
        "X-RapidAPI-Key": "7f60da8d4fmsh05a84112789f561p155c03jsnb0c39c42112a"
    }
    response = requests.request("GET", url, headers=headers, params=querystring)
    data = response.text
    return json.loads(data)

def ymmly_handler():
    ymmly = ymmly_list()
    #ymmly = json.ymmly_list()

    print(ymmly)   